Feature: DemoWebShop Automation

  Background: open Browser

  Scenario: Login Functionality
    When After launchig the URL
    Then Click on Login button
    And Enter the username "davidbilla07@gmail.com" in the textbox
    And Enter the password "David@07" in the textbox
    And click the log in button
    
   Scenario: Books Functionality
   	When After login click the books option
   	Then Click on sortBy and select high to low option
   	And scroll down the page
   	And Click on add to cart button of two productes
   	And scroll up the page
   	
   	Scenario: Electronics Functionality
   	When After adding two book products into cart click on electronics option
   	Then click on cellphone option
   	And click on add to cart for smartphone
   	And Display the total number of cart items
   	
   	Scenario: Gift card Functionality
   	When After displaying the cart items click on gift card option
   	Then click on display per page and make the option as four
   	And Fetch the name and price of any one gift card
   	And Display it
   	
   	Scenario: Logout Functionality
   	When click on logout button
   	Then Caputure the home page after log out
   	
   	
