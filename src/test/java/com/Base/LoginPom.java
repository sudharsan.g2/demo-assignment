package com.Base;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPom extends BaseClass{
	
	@FindBy (xpath ="//a[text()='Log in']")
	private WebElement lgbtn1;

	@FindBy (id = "Email")
	private WebElement usrnm;
	
	@FindBy (id = "Password")
	private WebElement pswrd;
	
	@FindBy (xpath ="//input[@class='button-1 login-button']")
	private WebElement lgbtn2;
	
	
	
	public void clickLoginBtn1() {
		lgbtn1.click();
	}
	
	public void enterUserName(String nm) {
		usrnm.sendKeys(nm);
	}
	
	public void enterPassword(String pwd) {
		pswrd.sendKeys(pwd);
	}
	
	public void clickLoginBtn2() {
		lgbtn2.click();
	}

}
