package com.Base;

import java.awt.Robot;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.Generic.Driverutils;

public class BaseClass {
	
	static WebDriver wd;
	static Robot r;
	
	public BaseClass() {
		wd = Driverutils.getDriver();
		PageFactory.initElements(wd, this);
	}
	
	public void teardown() {
		wd.quit();
	}

}
