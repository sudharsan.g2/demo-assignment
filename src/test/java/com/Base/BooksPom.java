package com.Base;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class BooksPom extends BaseClass{
	
	@FindBy (xpath ="//a[contains(text(),'Books')]")
	private WebElement bkopn;
	
	@FindBy (id ="products-orderby")
	private WebElement drpdwn;
	
	@FindBy (xpath ="(//input[@type=\"button\"])[4]")
	private WebElement adtocrt1;
	
	@FindBy (xpath ="(//input[@type=\"button\"])[5]")
	private WebElement adtocrt2;

	public void clickOnBooks() {
		bkopn.click();
	}
	
	public void clickOnDropDown() {

		Select sle = new Select(drpdwn);
		sle.selectByVisibleText("Price: High to Low");
	}
	
	public void scrollDown() {
		WebElement one = wd.findElement(By.xpath("//img[@alt=\"Picture of Computing and Internet\"]"));
		  JavascriptExecutor js = (JavascriptExecutor)wd;
		  js.executeScript("arguments[0].scrollIntoView(true)",one);
	}
	
	public void clickOnAddToCart1Btn() {
		adtocrt1.click();
	}
	
	public void clickOnAddToCart2Btn() {
		adtocrt2.click();
	}
	
	public void scrollUp() {
		WebElement two = wd.findElement(By.xpath("//div[@class='header']"));
		  JavascriptExecutor js = (JavascriptExecutor)wd;
		  js.executeScript("arguments[0].scrollIntoView(true)",two);
	}
	

}
