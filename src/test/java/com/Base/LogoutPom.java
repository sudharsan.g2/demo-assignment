package com.Base;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LogoutPom extends BaseClass{
	
	@FindBy (xpath ="//a[text()='Log out']")
	private WebElement lgout;
	

	public void clickOnLogout() {
		lgout.click();
	}
	
	public void capture() throws IOException {
		TakesScreenshot shot = (TakesScreenshot)wd;
		File source = shot.getScreenshotAs(OutputType.FILE);
		File destination = new File("/home/sudharsan/Music/Screenshot/Homepage.png");
		FileUtils.copyFile(source, destination);
	}

}
