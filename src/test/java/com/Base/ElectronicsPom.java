package com.Base;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class ElectronicsPom extends BaseClass{
	
	
	@FindBy (xpath ="//a[contains(text(),'Electronics')]")
	private WebElement eleopt;
	
	@FindBy (xpath ="//img[@alt='Picture for category Cell phones']")
	private WebElement celopt;
	
	@FindBy (xpath ="(//input[@type=\"button\"])[3]")
	private WebElement adtocrt;

	public void clickOnElectronics() {
		eleopt.click();
	}
	
	public void clickOnCellphones() {
		celopt.click();
	}
	
	public void clickOnAddToCart() {
		adtocrt.click();
	}
	
	
	public void totalCartQuantity() {
		WebElement qty = wd.findElement(By.xpath("//span[@class='cart-qty']"));
		String txt = qty.getText();
		System.out.println("The total No of product in the cart is: "+txt);
		
	}
	
	

}
