package com.Base;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class GiftcardsPom extends BaseClass{
	
	@FindBy (xpath ="//a[contains(text(),'Gift Cards')]")
	private WebElement gfcrd;
	
	@FindBy (id ="products-pagesize")
	private WebElement pgsize;
	
	@FindBy (xpath ="//img[@alt='Picture of $5 Virtual Gift Card']")
	private WebElement gft1;
	

	public void clickOnGiftcards() {
		gfcrd.click();
	}
	
	public void clickOnFirstGiftcards() {
		gft1.click();
	}
	
	public void selectDisplay() {
		Select sle = new Select(pgsize);
		sle.selectByVisibleText("4");
	}
	
	public void productTitle() {
		WebElement pro = wd.findElement(By.xpath("//h2[@class='product-title']"));
		String ttl = pro.getText();
		System.out.println("Gift Card Title is: "+ttl);
	}
	
	public void productPrice() {
		WebElement prc = wd.findElement(By.xpath("//span[@class='price actual-price']"));
		String amt = prc.getText();
		System.out.println("Gift Card Price is: "+amt);
	}
	
	public void capture() throws IOException {
		TakesScreenshot shot = (TakesScreenshot)wd;
		File source = shot.getScreenshotAs(OutputType.FILE);
		File destination = new File("/home/sudharsan/Music/Screenshot/Giftcard.png");
		FileUtils.copyFile(source, destination);
	}

}
