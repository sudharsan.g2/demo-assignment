package com.Steps;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.Base.BooksPom;

import io.cucumber.java.en.*;

public class BookSteps  {
	 WebDriver wd;
	BooksPom bp = new BooksPom();
	
	@When("After login click the books option")
	public void after_login_click_the_books_option() {
	    bp.clickOnBooks();
	}

	@Then("Click on sortBy and select high to low option")
	public void click_on_sort_by_and_select_high_to_low_option() {
		bp.clickOnDropDown();
	}

	@Then("scroll down the page")
	public void scroll_down_the_page() {
	  bp.scrollDown();
	}

	@Then("Click on add to cart button of two productes")
	public void click_on_add_to_cart_button_of_two_productes() throws InterruptedException {
	    bp.clickOnAddToCart1Btn();
	    Thread.sleep(2000);
	    bp.clickOnAddToCart2Btn();
	}

	@Then("scroll up the page")
	public void scroll_up_the_page() {
		bp.scrollUp();
	}

}
