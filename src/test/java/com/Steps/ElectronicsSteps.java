package com.Steps;

import com.Base.ElectronicsPom;

import io.cucumber.java.en.*;

public class ElectronicsSteps {
	
	ElectronicsPom ep = new ElectronicsPom();
	
	@When("After adding two book products into cart click on electronics option")
	public void after_adding_two_book_products_into_cart_click_on_electronics_option() {
	    ep.clickOnElectronics();
	}

	@Then("click on cellphone option")
	public void click_on_cellphone_option() {
	   ep.clickOnCellphones();
	}

	@Then("click on add to cart for smartphone")
	public void click_on_add_to_cart_for_smartphone() {
	   ep.clickOnAddToCart();
	}



	@Then("Display the total number of cart items")
	public void display_the_total_number_of_cart_items() {
	    ep.totalCartQuantity();
	}

}
