package com.Steps;

import java.io.IOException;

import org.openqa.selenium.WebDriver;

import com.Base.BaseClass;
import com.Base.LogoutPom;

import io.cucumber.java.en.*;

public class LogoutSteps {
	BaseClass bs = new BaseClass();
	LogoutPom lop = new LogoutPom();
	
	@When("click on logout button")
	public void click_on_logout_button() {
	   lop.clickOnLogout();
	}

	@Then("Caputure the home page after log out")
	public void caputure_the_home_page_after_log_out() throws IOException {
	   lop.capture();
	  bs.teardown();
	}

}
