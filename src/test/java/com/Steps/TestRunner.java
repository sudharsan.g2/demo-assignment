package com.Steps;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = "/home/sudharsan/Documents/DemoAssignement1/src/test/resources/features/Demo.feature"
		,glue="com.Step",
		plugin = {"html:target/cucumber-reportss"}
		
		)
public class TestRunner {

}
