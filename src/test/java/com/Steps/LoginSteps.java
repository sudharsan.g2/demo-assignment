package com.Steps;

import com.Base.LoginPom;

import io.cucumber.java.en.*;

public class LoginSteps {
	
	LoginPom lp = new LoginPom();
	
	@When("After launchig the URL")
	public void after_launchig_the_url() {
	   
	}

	@Then("Click on Login button")
	public void click_on_login_button() {
		lp.clickLoginBtn1();
	}

	@Then("Enter the username {string} in the textbox")
	public void enter_the_username_in_the_textbox(String string) {
	    lp.enterUserName(string);
	}

	@Then("Enter the password {string} in the textbox")
	public void enter_the_password_in_the_textbox(String string) {
	    lp.enterPassword(string);
	}

	@Then("click the log in button")
	public void click_the_log_in_button() {
	lp.clickLoginBtn2();   
	}

}
