package com.Steps;

import java.io.IOException;

import com.Base.GiftcardsPom;

import io.cucumber.java.en.*;

public class GiftcardsSteps {
	
	GiftcardsPom gp = new GiftcardsPom();
	
	@When("After displaying the cart items click on gift card option")
	public void after_displaying_the_cart_items_click_on_gift_card_option() {
	    gp.clickOnGiftcards();
	}

	@Then("click on display per page and make the option as four")
	public void click_on_display_per_page_and_make_the_option_as_four() {
	   gp.selectDisplay();
	}

	@Then("Fetch the name and price of any one gift card")
	public void fetch_the_name_and_price_of_any_one_gift_card() {
	   gp.productTitle();
	   
	   gp.productPrice();
	}

	@Then("Display it")
	public void display_it() throws IOException, InterruptedException {
	    gp.clickOnFirstGiftcards();
	    Thread.sleep(2000);
	    gp.capture();
	}

}
