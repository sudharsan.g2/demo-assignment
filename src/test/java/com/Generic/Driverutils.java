package com.Generic;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Driverutils {
	
	static WebDriver wd;
	public static void launchbrowser() {
		System.setProperty("webdriver.chrome.driver","/home/sudharsan/Downloads/chromedriver_linux64 (1)/chromedriver");
		 wd = new ChromeDriver();
		 wd.get("https://demowebshop.tricentis.com/login");
		wd.manage().window().maximize();
		wd.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS); 
	}
	public static WebDriver getDriver()
	{
		if(wd==null) {
			launchbrowser();
		}
		return wd;
	}

}
